package com.example.todoapp.model

data class ToDoList(
    var id: Long = 0,
    var name: String,
    var items: List<ToDoItem> = emptyList()
)
