package com.example.todoapp

import android.content.Context
import android.os.Bundle
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity

class SettingsActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val sharedPreferences = getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
        val completedColorRadioGroup = findViewById<RadioGroup>(R.id.completed_color_radio_group)
        val notCompletedColorRadioGroup = findViewById<RadioGroup>(R.id.notCompleted_color_radio_group)

        when (sharedPreferences.getString("selected_color1", "#FFFFFF")) {
            "#00FF00" -> findViewById<RadioButton>(R.id.green).isChecked = true
            "#0000FF" -> findViewById<RadioButton>(R.id.blue).isChecked = true
            "#FFFF00" -> findViewById<RadioButton>(R.id.yellow).isChecked = true
        }

        completedColorRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            val selectedColor1 = when (checkedId) {
                R.id.green -> "#00FF00"
                R.id.blue -> "#0000FF"
                R.id.yellow -> "#FFFF00"
                else -> "#FFFFFF"
            }
            sharedPreferences.edit().putString("selected_color1", selectedColor1).apply()
        }

        when (sharedPreferences.getString("selected_color2", "#FFFFFF")) {
            "#B4513" -> findViewById<RadioButton>(R.id.brown).isChecked = true
            "#FFC0CB" -> findViewById<RadioButton>(R.id.pink).isChecked = true
            "#EE82EE" -> findViewById<RadioButton>(R.id.violet).isChecked = true
        }

        notCompletedColorRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            val selectedColor2 = when (checkedId) {
                R.id.brown -> "#B4513"
                R.id.pink -> "#FFC0CB"
                R.id.violet -> "#EE82EE"
                else -> "#FFFFFF"
            }
            sharedPreferences.edit().putString("selected_color2", selectedColor2).apply()
        }
    }
}
