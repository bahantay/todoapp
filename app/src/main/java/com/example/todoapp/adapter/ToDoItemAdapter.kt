package com.example.todoapp.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.model.ToDoItem

class ToDoItemAdapter :
    RecyclerView.Adapter<ToDoItemAdapter.ItemViewHolder>() {

    private val toDoItem = mutableListOf<ToDoItem>()
    var onItemEditClick: ((ToDoItem) -> Unit)? = null
    var onItemDeleteClick: ((ToDoItem) -> Unit)? = null

    fun submitItem(newItem: List<ToDoItem>) {
        toDoItem.clear()
        toDoItem.addAll(newItem)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.content_item, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.onBind(toDoItem[position])
    }

    override fun getItemCount(): Int = toDoItem.size

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val toDoItemName: TextView = view.findViewById(R.id.itemName)
        private val completedCheckBox: CheckBox = view.findViewById(R.id.completed)
        private val editItem: ImageButton = view.findViewById(R.id.editItem)
        private val deleteItem: ImageButton = view.findViewById(R.id.deleteItem)

        fun onBind(item: ToDoItem){
            toDoItemName.text = item.name
            completedCheckBox.isChecked = item.completed
            completedCheckBox.setOnCheckedChangeListener { _, isChecked ->
                item.completed = isChecked
                val sharedPreferences =
                    itemView.context.getSharedPreferences("my_prefs", Context.MODE_PRIVATE)
                val selectedColor1 = sharedPreferences.getString("selected_color1", "#FFFFFF")
                val selectedColor2 = sharedPreferences.getString("selected_color2", "#FFFFFF")

                val color = if (isChecked) selectedColor1 else selectedColor2

                toDoItemName.setTextColor(Color.parseColor(color))
            }

            editItem.setOnClickListener {
                onItemEditClick?.invoke(item)
            }

            deleteItem.setOnClickListener {
                onItemDeleteClick?.invoke(item)
            }
        }
    }
}