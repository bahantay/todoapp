package com.example.todoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.adapter.ToDoListAdapter
import com.example.todoapp.viewModel.ToDoListViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private lateinit var toDoListAdapter: ToDoListAdapter
    private lateinit var toDoListViewModel: ToDoListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "Dashboard"
        setSupportActionBar(toolbar)

        val settings = findViewById<Button>(R.id.settings)
        settings.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }

        val listRV = findViewById<RecyclerView>(R.id.todoList)

        toDoListViewModel = ViewModelProvider(this)[ToDoListViewModel::class.java]

        toDoListAdapter = ToDoListAdapter()
        listRV.adapter = toDoListAdapter
        listRV.layoutManager = LinearLayoutManager(this)

        toDoListAdapter.onListClick = {
            val intent = Intent(this, ItemActivity::class.java)
            intent.putExtra("listId", it.id)
            startActivity(intent)
        }

        toDoListAdapter.onListEditClick = {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Edit ToDo list")

            val layout = LinearLayout(this)
            layout.orientation = LinearLayout.VERTICAL
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            val margin = resources.getDimensionPixelSize(R.dimen.dialog_margin)
            layoutParams.setMargins(margin, margin, margin, margin)

            val editText = EditText(this)
            editText.inputType = InputType.TYPE_CLASS_TEXT
            editText.setText(it.name)
            layout.addView(editText, layoutParams)

            builder.setView(layout)

            builder.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }

            builder.setPositiveButton("Update") { _, _ ->
                val name = editText.text.toString().trim()
                if (name.isNotEmpty()) {
                    it.name = name
                    toDoListViewModel.updateList(it)
                } else {
                    Toast.makeText(this, "Please enter a name", Toast.LENGTH_SHORT)
                        .show()
                }
            }

            builder.create().show()
        }

        toDoListAdapter.onListDeleteClick = {
            val alertDialog = AlertDialog.Builder(this)
                .setTitle("Delete List")
                .setMessage("Are you sure to delete this list?")
                .setPositiveButton("Delete") { _, _ ->
                    toDoListViewModel.deleteList(it)
                }
                .setNegativeButton("Cancel", null)
                .create()
            alertDialog.show()
        }

        toDoListViewModel.allToDoLists.observe(this) {
            toDoListAdapter.submitList(it)
        }

        val fab = findViewById<FloatingActionButton>(R.id.fab)

        fab.setOnClickListener {
            val builder = AlertDialog.Builder(this)

            builder.setTitle("Add ToDo list")

            val layout = LinearLayout(this)
            layout.orientation = LinearLayout.VERTICAL
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            val margin = resources.getDimensionPixelSize(R.dimen.dialog_margin)
            layoutParams.setMargins(margin, margin, margin, margin)

            val editText = EditText(this)
            editText.inputType = InputType.TYPE_CLASS_TEXT
            editText.hint = "Enter name"
            layout.addView(editText, layoutParams)

            builder.setView(layout)

            builder.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }

            builder.setPositiveButton("Add") { dialog, _ ->
                val name = editText.text.toString().trim()

                if (name.isNotEmpty()) {
                    toDoListViewModel.addList(name)
                    dialog.dismiss()
                } else {
                    Toast.makeText(this, "Please enter a name", Toast.LENGTH_SHORT).show()
                }
            }

            builder.create().show()
        }
    }
}